import { Component, OnInit, Input, Type } from '@angular/core';



export class Post {

  title: string;
  content: string;
  loveIts: number;
  created_at: Date;

  constructor(unTitle: string, unContent: string) {
    this.title = unTitle;
    this.content = unContent;
    this.loveIts = 0;
    this.created_at = new Date();
  }


}
