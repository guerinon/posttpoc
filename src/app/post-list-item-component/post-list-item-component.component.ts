import { Component, OnInit, Input, Type } from '@angular/core';
import { Post } from '../PostClassModel';

@Component({
  selector: 'app-post-list-item-component',
  templateUrl: './post-list-item-component.component.html',
  styleUrls: ['./post-list-item-component.component.css']
})



export class PostListItemComponentComponent implements OnInit {
  @Input() post: Post;


  constructor() { }

  ngOnInit() {
  }

  onClickLike(unPost: Post) {
    this.post.loveIts += 1;
    console.log(this.post.loveIts);
  }

  onClickDislike(unPost: Post) {
    this.post.loveIts -= 1;
  }

}
