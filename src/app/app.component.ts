import { Component } from '@angular/core';
import { Post } from './PostClassModel';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Posts App';


  post1 = new Post('Post1', 'Lorem ipsum dolor sit amet, consectetur adaliqua. Ut enim ad minim veniam');
  post2 = new Post('Post2', 'Lorem ipsum dolor sit amet, consectetur adaliqua. Ut enim ad minim veniam');
  post3 = new Post('Post3', 'Lorem ipsum dolor sit amet, consectetur adaliqua. Ut enim ad minim veniam');
  post4 = new Post('Post4', 'Lorem ipsum dolor sit amet, consectetur adaliqua. Ut enim ad minim veniam');
  posts = [this.post1, this.post2 , this.post3, this.post4];


}
